public class Student {
    private int ects_dlug;
    private char[] indexNumber;
    public int getEcts_dlug() {
        return ects_dlug;
    }
    public void setEcts_dlug(int _ects)
    {
        ects_dlug = _ects;
    }
    public char[] getIndexNumber() {
        return indexNumber;
    }
    public void setIndexNumber(char[] _index)
    {
        indexNumber = _index;
    }
    public Student()
    {
        setEcts_dlug(0);
        setIndexNumber(null);
    }
    public void oblej()
    {
        setEcts_dlug(Integer.MAX_VALUE);
    }
}
